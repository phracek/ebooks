#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2013-2014 Red Hat, Inc.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# he Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
# Authors:  Petr Hracek <phracek@redhat.com>

import os
import sys
table = {'á':'&#xe1;',
'é': '&#xe9;',
'í': '&#xed;',
'ý': '&#xfd;',
'ž': '&#382;',
'ř': '&#345;',
'č': '&#269;',
'š': '&#353;',
'ě': '&#283;',
'ů': '&#367;',
'ú': '&#xfa;',
'ň': '&#328;',
'ó': '&#243;',
'Ó': '&#211;',
'Á': '&#xc1;',
'Í': '&#xcd;',
'É': '&#xc9;',
'Ý': '&#221;',
'Ž': '&#381;',
'Ř': '&#344;',
'Č': '&#268;',
'Š': '&#352;',
'Ě': '&#282;',
'Ů': '&#366;',
'Ú': '&#218;',
'Ň': '&#327;'
         }

f_content = None
with open(sys.argv[1], 'r') as f:
    f_content = f.read()
    for item in table.keys():
        f_content = f_content.replace(item, table[item])
    with open(sys.argv[1] + '_new', "w") as f_out:
        f_out.write(f_content)