# ebooks
This repository contains several catholic e-books

## DOCUMENTS OF THE SECOND VATICAN COUNCIL
These documents are taken from URL: http://www.vatican.va/archive/hist_councils/ii_vatican_council/index.htm

## SUMMA THEOLOGIAE
These documents are taken from URL: http://www.cormierop.cz/SVTOMAS-AKVINSKY.html

## How to add more documents
If you would like to add more document,
just add a new issue https://github.com/phracek/ebooks/issues with URL
and I will try to add the document as book as soon as possible.

## How to support ebooks repository
Just mark this repository by Star. Nothing more, nothing else.